from IronWASP import *
from System import *
from System.Xml import *
from System.Text import *
from System.IO import *
import clr
import binascii


#Inherit from the base FormatPlugin class
class protoBuffDecoder(FormatPlugin):

  #Override the ToXmlFromRequest method of the base class with custom functionlity. Convert RequestBody in to Xml String and return it
  def ToXmlFromRequest(self, req):
    bodyString = Tools.ToHex(req.BodyString)
    bodyString = "".join(bodyString.split())
    n = 2 
    bodyArray = [bodyString[i:i+n] for i in range(0, len(bodyString), n)]
    firstByte = bodyArray[0]
    binaryFirstByte = (bin(int(firstByte,16))[2:]).zfill(8)
    wireType = binaryFirstByte[5:]
    if wireType == '010':
      lengthByte = int(bodyArray[1],16)
      istr = bodyString[4:]
      #hexistr = ''.join( "%"+i+istr[n+1] for n,i in enumerate(istr)  if n%2==0 )
      xml = '<xml>'
      xml += '<' + "dummy" + '>'
      #xml += Tools.XmlEncode(Tools.HexDecode(hexistr))
      xml += Tools.XmlEncode(binascii.unhexlify(istr))
      xml += '</' + "dummy" + '>'
      xml += '<' + "WireType" + '>'
      xml += Tools.XmlEncode(wireType)
      xml += '</' + "WireType" + '>'
      xml += '</xml>'
    #xml = '<xml>'
    return xml


  #Override the ToRequestFromXml method of the base class with custom functionlity. Update Request based on Xml String input and return it
  def ToRequestFromXml(self, req, xml):
    pass


p = protoBuffDecoder()
p.Name = "Protobuff Decoder"
p.Description = "Format Plugin to Handle the Protobuff"
FormatPlugin.Add(p)

